// @version 20210326
// @version 20210623 @douglascox implemented refactored go/give


// function checkMergedRanges(ss) {
//   // var ss = SpreadsheetApp.getActiveSpreadsheet()
//   var sheet = ss.getSheetByName("Incentives");
//   var ranges = ["B10:E10", "B9:E9", "B8:E8"]
//   var isCurrVersion = sheet.getRange(1, 1).getValue() == "INSTRUCTIONS"
//   isCurrVersion && reMerge()

//   function reMerge() {
//     ranges.filter(isUnMerged)
//       .map(toMerge)
//   }
//   function isUnMerged(a1) { return !sheet.getRange(a1).isPartOfMerge() }
//   function toMerge(a1) { return sheet.getRange(a1).merge() }
// }
function anon() {
  const spreadSheetId = '1QXQfApiIXzFdQVM9zGMRd3tEnJ0oLI-HiQyYe-RpR9w';
  const id = "1kCTfeEQVgnEn6NNxYWUucrH5Auho-JfQOrkFUkWyj5A"
  const ss = SpreadsheetApp.openById(spreadSheetId)
  const sheet = ss.getSheetByName("Incentives")
  processIncentives({ sheet, sessionEmail: "srothman@google.com" })
}

function processIncentives({ sheet, sessionEmail }) {
  let spreadsheet = sheet.getParent()
  const ldap = sessionEmail.substring(0, sessionEmail.indexOf("@google"))
  const sessionLdap = sessionEmail.substring(0, sessionEmail.indexOf("@google"))
  //load in current sheet
  let rows = sheet.getDataRange();
  let values = rows.getValues();
  let sheetName = sheet.getName();

  const studyname = values[1][0]
  if (["Study name here", ""].includes(studyname)) {
    let message = (`Please update "Study name" at cell A2`)
    Browser.msgBox(message)
    throw (message)
  };

  values = removeNaN(values)
  const { USsheetObj, INTsheetObj, INToptionSheetObj, INTlanguageSheetObj, massageSheetObj, } = getSelectedSheets()
  const USsheet = USsheetObj.sheet
  const INTsheet = INTsheetObj.sheet;
  const INTlanguageSheet = INTlanguageSheetObj.sheet
  const massageSheet = massageSheetObj.sheet
  // const apacNonPerksSheet = APACSheetObj.sheet
  const usName = USsheet.getName();
  const ints = INTsheet.getName();
  const mshe = massageSheet.getName();

  // studyLocation is cell refenced by studyName
  // researcherLocation is cell referenced by UXR's Corp Email
  const studyNameField = values[1][2] || "";
  const isValidStudyName = ["Study name here", ""].some(str => str === studyNameField.trim())
  if (!isValidStudyName) {
    let message = ("Please update study name at cell B2")
    Browser.msgBox(message)
    throw (message)
  }
  const { headerRow, startRow, researcherLocation, studyLocation, studyId } = getHeaderStartRow(values);
  const uxrCorpEmail = researcherLocation || ""
  Logger.log(`UXR email: ${researcherLocation}`)

  const isResearcherEmailInvalid = (uxrCorpEmail.indexOf("[") >= 0) || (researcherLocation == "")
  conditionalEscape(isResearcherEmailInvalid, "Researcher email is not valid.  Cannot continue.");
  //Look up researcher 

  let [researcherID, businessUnit, errorString] = getResearcherID(researcherLocation, undefined, "")
  // errorString populated by above function if errors string caught
  conditionalEscape(errorString != "", errorString)

  //  let [researcherID, businessUnit, errorString] = ["researcherId", "[n/a]business unit", ""]
  // researcher validators
  conditionalEscape((errorString != ""), errorString)

  // Da loop
  const { body, headers, headerObj } = getDataBySheet(sheet, headerRow, startRow)
  // const dataBySheet = getDataBySheet(sheet, 10, 12)

  // conditional to check row has not already been process
  // inverse the validation from the below to not being ""
  // processConditional cp from processIncentives Fn
  const isRedHighlight = (rowAsObj => rowAsObj.getRangeByColHeader("Processed Status").getFontColor() === "#660000")
  const isGreenHighlight = (rowAsObj) => {
    return rowAsObj.getRangeByColHeader("Processed Status").getFontColor() === "#38761d"
  }
  const isEmpty = (rowAsObj => rowAsObj["Processed Status"] === "")
  const isEmptyRow = (rowAsObj => Object.keys(rowAsObj).every(header => rowAsObj[header] === ""))


  const processConditional = (rowAsObj) => {
    const processedStatus = rowAsObj["Processed Status"];
    const hasExample = processedStatus.includes("example");
    const hasAtSymbol = processedStatus.includes('@');
    const notEvaluate = isEmptyRow(rowAsObj);
    const isGreenRow = isGreenHighlight(rowAsObj);
    const hasFailCriteria = (!isGreenRow && !hasAtSymbol);
    const rowToProcessed = (notEvaluate || hasExample) ? false : hasFailCriteria
    return rowToProcessed
  }
  const blankProps = (props) => !headers.filter(str => str !== "").every(header => props[header] === "")

  const processCodes = body.filter(processConditional)
    .filter(blankProps)
  Logger.log(JSON.stringify({ headerRow, startRow, researcherLocation, studyLocation, studyId }));
  // debugger
  let rowsWithErrors = []
  // return ""
  processCodes.forEach((rowAsObj) => {
    errorString = ""
    //check if incentive type is specific
    const incentiveType = rowAsObj["Incentive"];
    const currencyType = rowAsObj["Currency"];
    const preferredLanguage = rowAsObj["Participant Preferred Language"];
    const countryAbbrev = rowAsObj["Participant Country Code"];
    const participantName = rowAsObj["Participant Name"]
    const perksCodeAmount = rowAsObj["Perks Code Amount"]
    const participantEmail = rowAsObj["Participant Email"]
    const participantLdap = participantEmail.substring(0, participantEmail.indexOf("@"))
    const catalogType = rowAsObj["Catalog Type"]
    const sessionType = rowAsObj.hasOwnProperty("Session Type") ? rowAsObj["Session Type"] : "Session Type Not Found"
    if (incentiveType === "") { errorString += "Incentive type is missing. \n" }
    if (participantEmail === "") { errorString += "Email Address is not valid. \n" }
    const userGroupMapping = {
      "External": "NR",
      "Internal": "INT",
      "External (Restricted)": "RS",
      "": ""
    }
    const hasValidCatalogType = Object.keys(userGroupMapping).includes(catalogType)
    !hasValidCatalogType && (errorString += `Not a valid Catalog Type: "${catalogType}"`)

    const userGroup = userGroupMapping.hasOwnProperty(catalogType) ? userGroupMapping[catalogType] : "NOT APPLICABLE"
    validatePartipantEmail(rowAsObj, userGroup)

    const isPersonTVC = (rowAsObj) => hasAtGoogle(rowAsObj) ? isTVC(rowAsObj) : false;
    const processGoGive = _processGoGive({ participantLdap, researcherLocation, participantEmail, uxrCorpEmail, sessionEmail, researcherID, businessUnit, spreadsheet, studyId, sessionLdap })
    if (errorString === '') {
      try {
        processByIncentiveType(rowAsObj)
      } catch (e) { errorString += `Row failed: ${e}` }
      // isPerks ? processAsPerks(rowAsObj) : isMassage ? processAsMassage(rowAsObj) : (() => { throw "incentive type not recognized" })
      const hasRowErrors = !errorString.includes(sessionEmail)
      Logger.log(`Session email check: ${sessionEmail}`);
      const color = hasRowErrors ? "#660000" : "#38761d"
      debugger
      if (errorString.length > 0) {
        rowAsObj.getRangeByColHeader("Processed Status").setValue(errorString).setFontColor(color);
      }
      let currentRowProcess = rowAsObj.getRangeByColHeader("Processed Status").getRow();
      hasRowErrors && rowsWithErrors.push(currentRowProcess)

    } // noteBy:douglascox not sure who put this if statement here, original execution flow didn't include

    SpreadsheetApp.flush()

    function processByIncentiveType(rowAsObj) {
      // const processGoGiveByRowAsObj = processGoGive({ participantLdap, researcherLocation, participantEmail,  uxrCorpEmail, errorString, sessionEmail, researcherID, businessUnit, spreadsheet, studyId, sessionLdap})
      // const processGoGiveByRowAsObj = processGoGive(rowContext)
      const incentiveTypes = {
        "massage": processAsMassage,
        "perks": processAsPerks,
        "go/give rewards": processGoGive
        // 2021.03.26 removed
        // "apac non perks": processAsApacNonPerks,
      }
      const formatIncetiveType = (incentiveType) => incentiveType.toLowerCase().trim()
      let incentiveType = formatIncetiveType(rowAsObj["Incentive"])
      const boo = incentiveTypes.hasOwnProperty(incentiveType)
      try {
        const temp = incentiveTypes[incentiveType]
      } catch (e) {
        return (`Incentive Type ${incentiveType} not found /must be ${Object.keys(incentiveTypes).join(", ")}`)
      }
      // debugger

      return incentiveTypes.hasOwnProperty(incentiveType) ? incentiveTypes[incentiveType](rowAsObj) : (() => { errorString += `incentive type: "${incentiveType}" not recognized  \n` })()
    }

    function processAsApacNonPerks(rowAsObj) {
      if (errorString !== "") { return }
      hasAtGoogle(rowAsObj) && (errorString += "Googlers may not receive APAC incentives. \n");
      const apacIncentiveAmt = rowAsObj["APAC Incentive Amount"]
      const apacCurrencyType = rowAsObj["Currency (APAC)"]
      const row = [ldap, Utilities.formatDate(new Date(), "PST", "MM/dd/yyyy ' ' HH:mm:ss"), participantName, participantEmail, apacIncentiveAmt, apacCurrencyType, studyId, studyId, researcherID, businessUnit, spreadsheet.getUrl(), sessionType]
      /*
      Recruiter ldap	Entry Date	Participant First & Last Name	Email Address	Incentive Amount	"Currency Type
     (AUD or INR)"	Study ID	Study ID	Researcher ID	Business Unit	Study Spreadsheet	Session Type
      */
      if (errorString == "") {
        errorString = sessionEmail + " " + Utilities.formatDate(new Date(), "PST", "MM/dd/yyyy ' ' HH:mm:ss") + " ";
        Logger.log(`APAC incentives updated ${row}`)
        // Logger.log(`appendRow apac commented out`)
        // apacNonPerksSheet.appendRow(row)
        Logger.log(`updating @ ${APACSheetObj.ss.getUrl()}`)
      }
      debugger
    }

    function processAsMassage(rowAsObj) {
      checkForMissingValues(rowAsObj, ["Participant Name", "Participant Email", "Incentive",
        "# of massage points requested"])

      const massagePoints = rowAsObj["# of massage points requested"]
      isNumber(massagePoints) && (errorString += "# of massage points requested must be a number. \n");
      !hasAtGoogle(rowAsObj) && (errorString += "Only Googlers can receive massage credits. \n");
      const getLdap = (str) => str.indexOf("@google.com") ? str.substring(0, str.indexOf("@google.com")) : false;
      const participantLdap = hasAtGoogle(rowAsObj) ? participantEmail.substring(0, participantEmail.indexOf("@google.com")) : false;
      !ldap && (errorString += "Recipient does not appear to be a valid Googler. \n")


      // const row = [sessionEmail,]
      if (errorString === "") {
        const massagePoints = rowAsObj["# of massage points requested"];
        errorString = sessionEmail + " " + Utilities.formatDate(new Date(), "PST", "MM/dd/yyyy ' ' HH:mm:ss") + " ";
        // no ldap on Usability massage awareds
        // const row = [ldap, Utilities.formatDate(new Date(), "PST", "MM/dd/yyyy ' ' HH:mm:ss"), participantName, massagePoints, studyId, researcherID, businessUnit, spreadsheet.getUrl(), sessionType]
        const row = [sessionLdap, Utilities.formatDate(new Date(), "PST", "MM/dd/yyyy ' ' HH:mm:ss"), studyId, researcherID, businessUnit, spreadsheet.getUrl(), sessionType, participantName, participantLdap, massagePoints,]
        // throw ('massage row not sync with spreadsheet')
        Logger.log(`Massage updated INT  row ${rowAsObj.getRowIdx}, @ sheetName: ${massageSheet.getName()} url:${massageSheetObj.ss.getUrl()}`)
        // Logger.log(`appendRow massageSheet commented out`)
        massageSheet.appendRow(row);
      }
      debugger
    }

    function processAsPerks(rowAsObj) {
      checkForMissingValues(rowAsObj, ["Participant Name", "Participant Email", "Incentive",
        "Currency", "Perks Code Amount", "Participant Country Code",
        "Participant Preferred Language"])
      isNumber(perksCodeAmount) && (errorString += "Perks code amount must be a number. \n");
      const country = rowAsObj["Participant Country Code"]

      const dataByCountryCurrencySheet = getDataBySheet(INToptionSheetObj.sheet)
      const countryByAbbr = dataByCountryCurrencySheet.body.find((rowAsObj) => rowAsObj["Abbreviation"] === country);
      const countryFound = !!countryByAbbr // 0false 1true  
      !countryFound && (errorString += `Participant Country Code: "${country}" not found  \n`)
      const cat = catalogType
      const catalogTypeInternalError = (catalogType === "Internal" && !hasAtGoogle(rowAsObj));
      if (catalogTypeInternalError) { errorString += `${catalogType} not valid for ${participantEmail}`; }

      debugger

      const hasErrorInPerksParam = errorString !== ""
      const evaulatePerk = () => (country == "US" || country == "us" ? perksForUS() : perksInt())

      !hasErrorInPerksParam && evaulatePerk()


      function perksForUS() {
        isSame("USD", rowAsObj["Currency"], "Currency for US should be USD. \n")
        isSame("English", rowAsObj["Participant Preferred Language"], "Language for US should be English. \n")
        //  userGroup = getUserGroup(rowAsObj);
        updatePerksSheetForUS()
        // check if Perks has locked down sheet 
        function updatePerksSheetForUS() {
          var protections = USsheet.getProtections(SpreadsheetApp.ProtectionType.SHEET)[0];
          var canEdit = 0;
          if (protections) {
            try {
              var editors = protections.getEditors();
            }
            catch (e) {
              errorString = errorString + "Perks is currently processing our request list so new requests are temporarily suspended. Try submitting yours again at a later time. \n";
            }
          }
          debugger
          if (errorString === "") {
            errorString = sessionEmail + " " + Utilities.formatDate(new Date(), "PST", "MM/dd/yyyy ' ' HH:mm:ss") + " ";
            var row = [ldap, Utilities.formatDate(new Date(), "PST", "MM/dd/yyyy ' ' HH:mm:ss"), participantName, participantEmail, perksCodeAmount,
              userGroup, studyId, researcherID, businessUnit, spreadsheet.getUrl(), sessionType]

            Logger.log(`updating perks USA  row ${rowAsObj.getRowIdx}, @ sheetName: ${USsheet.getName()} url: ${USsheetObj.ss.getUrl()}`)
            // Logger.log(`appendRow USsheet commented out`)
            USsheet.appendRow(row);
          }
        }
        debugger
      }

      function perksInt() {
        // const dataByCountryCurrencySheet = getDataBySheet(INToptionSheetObj.sheet)
        // const countryByAbbr = dataByCountryCurrencySheet.body.find((rowAsObj) => rowAsObj["Abbreviation"] === country);
        // const countryFound = !!countryByAbbr // 0false 1true  
        // debugger
        // if (!countryFound) {
        //   errorString += `country: ${country} not found`;
        // }
        debugger
        /** 2021.03.26 update the followig column header refereces
         * "Promocode Currency Options" to"Possible Currency Options"
         *        "Suggested Currency" to "Recommended Currency"
         */
        const countryExcluded = /excluded/gi.test(countryByAbbr["Promocode Currency Options"]);
        const isValidPossibleCurrency = countryByAbbr["Promocode Currency Options"].includes(currencyType);
        const isValidCurrencyRecommended = countryByAbbr["Suggested Currency"].includes(currencyType);

        countryExcluded && (errorString += `${country} has been exluded from processing. \n`)
        !(isValidPossibleCurrency || isValidCurrencyRecommended) && (errorString += `The currency "${currencyType}" is not the "Suggested Currency" for this country: "${country}"". \n`);
        debugger

        const dataByCountryLanguageSheet = getDataBySheet(INTlanguageSheet);
        const matchLanguageCurrency = dataByCountryLanguageSheet.body.find((rowAsObj) => rowAsObj["Currency"] === currencyType);
        const currencyFound = !!matchLanguageCurrency;
        const isValidLanguageCurrency = matchLanguageCurrency["Language Options"].includes(preferredLanguage)
        Logger.log(`isValidLanguage: ${isValidLanguageCurrency}`)
        Logger.log(`preferredLanguage: ${preferredLanguage}`)
        Logger.log(`language options: ${matchLanguageCurrency["Language Options"]}`)
        const isNotValidLanguageCurrency = !isValidLanguageCurrency

        !!isNotValidLanguageCurrency && (errorString += "This language cannot be used with this currency. \n");

        !countryFound && (errorString += `Participant Country Code "${country}" cannot be found. \n`);
        !currencyFound && (errorString += `Currency: "${currencyType}" cannot be found. \n`);

        // removed 4.9.21 https://buganizer.corp.google.com/issues/184971901
        // hasAtGoogle(rowAsObj) && (errorString += `Only US Googlers may receive Perks Code. \nParticipant Country Code must be "US" currently is ${country} `);

        if (errorString == "") {
          errorString = sessionEmail + " " + Utilities.formatDate(new Date(), "PST", "MM/dd/yyyy ' ' HH:mm:ss") + " ";

          var row = [ldap, Utilities.formatDate(new Date(), "PST", "MM/dd/yyyy ' ' HH:mm:ss"), participantName, participantEmail,
            currencyType, perksCodeAmount, country, preferredLanguage, studyId, researcherID, businessUnit, spreadsheet.getUrl(), sessionType]
          Logger.log(`updating row ${rowAsObj.getRowIdx}, perks INT @ sheetName: ${INTsheet.getName()} url: ${INTsheetObj.ss.getUrl()}`)
          // Logger.log(`appendRow INTsheet commented out`)
          INTsheet.appendRow(row);
        }
        debugger
      }
    }


    function isSame(left, right, msg) { left !== right && (errorString += msg) }
    function isNumber(num) { return isNaN(parseInt(num)) }
    function checkForMissingValues(rowAsObj, columnList) {
      columnList.filter(header => rowAsObj[header] === "")
        .forEach((header) => errorString += `${header} is missing. \n`)
    }

    /** added 6/22/2021 */
    function _processGoGive({ participantLdap, researcherLocation, participantEmail, uxrCorpEmail, sessionEmail, researcherID, spreadsheet, studyId, sessionLdap, }) {
      /** find user data */
      const findByldap = (data) => (str) => data.find(({ ldap }) => ldap === str)
      /** whom we support stuff */
      const whomWeSupportPros = fetchSheetProps("1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A", (ss) => ss.getSheetByName("All ldaps"))
      const { headers: whomWeSupportHeaders, body: whomWeSupportBody } = whomWeSupportPros
      let researcherLdap = uxrCorpEmail.slice(0, uxrCorpEmail.indexOf('@'))
      const researchWhomWeSupportData = findByldap(whomWeSupportBody)(researcherLdap)
      /** go give stuff */
      const goGiveProps = fetchSheetProps("1RhbCDaCeZi--ui1lxAYCkZ5DhzM10-TW9Wkbw9dbRJ4", (ss) => ss.getSheets()[0]);
      const { headers: goGiveHeader, sheet: goGiveSheet } = goGiveProps
      /** research stuff */
      const researcherEmail = researcherLocation;

      // const researchEmployeeId = getEmployeeID(researcherEmail);

      return function (rowAsObj) {
        const validateGoGiveRow = (props, msg, cb) => {
          if (cb(props)) {
            errorString += msg //
            return false
          } else {
            return true
          }
        }

        const hasGoGiveAmount = validateGoGiveRow(rowAsObj, "Failure: No go/give amount entered", (props) => props["Amount"] === "");
        const isNotTVC = validateGoGiveRow(rowAsObj, "Failure: Participant is TVC", (props) => isTVC(props));
        const isValidGoGive = (props) => [hasGoGiveAmount, isNotTVC].every(boolean => boolean);

        return isValidGoGive(rowAsObj) ? addParticipantToGoGive(rowAsObj) : null

        function addParticipantToGoGive(props) {
          const objHasProp = (props, str) => props.hasOwnProperty(str) ? props[str] : null
          const employeeIdOfRecipent = getEmployeeID(participantEmail)
          // const bu = researchWhomWeSupportData["Business Unit"]
          let getGoGiveValues = {
            "Recruiter ldap": sessionLdap,
            "Entry Date": new Date(),
            "Study ID": studyId,
            "Researcher ID": objHasProp(researchWhomWeSupportData, "Researcher ID"),
            "Business Unit": objHasProp(researchWhomWeSupportData, "Business Unit"),
            "Study Spreadsheet": spreadsheet.getUrl(),
            "Session Type": objHasProp(props, "Session Type"),
            "Participant Type": "Internal",
            "Recipient Name": objHasProp(props, "Participant Name"),
            "Recipient LDAP": participantLdap,
            "Employee ID": employeeIdOfRecipent,
            "Reward Amount": objHasProp(props, "Reward Amount"),
            "Location": objHasProp(props, "Office Location (Googler)*"),
            "Status": "Unuploaded"
          };
          const setGoGiveValues = str => getGoGiveValues.hasOwnProperty(str) ? getGoGiveValues[str] : ""
          let values = goGiveHeader.map(setGoGiveValues);
          goGiveSheet.appendRow(values);
          let tempers = errorString
          errorString += sessionEmail + " " + Utilities.formatDate(new Date(), "PST", "MM/dd/yyyy ' ' HH:mm:ss") + " ";
          debugger
          //
        }
      }

      function fetchSheetProps(id, cb) {
        const spreadsheet = SpreadsheetApp.openById(id);
        const sheet = cb(spreadsheet)
        const { body, headers } = getDataBySheet(sheet);
        return {
          sheet, headers, body
        }
      };
      function getEmployeeID(str) {
        var person = {};
        try {
          var resp = AdminDirectory.Users.get(str, { 'viewType': 'domain_public', 'projection': 'full' });

          if (resp != null) {
            var p = resp;
            person.employeeId = getExternalId(p.externalIds, "Employee ID");
            return person.employeeId;
          }
        } catch (e) {
          return null;
        }
      };

    }
  })

  // rowsWithErrors.length !== 0 && Browser.msgBox(`The following rows Fails ${rowsWithErrors.join(', ')}`)
  rowsWithErrors.length !== 0 && Logger.log(`The following rows Fails ${rowsWithErrors.join(', ')}`)

  // SpreadsheetApp.getActiveSpreadsheet().toast('Incentives Process Finished');
  SpreadsheetApp.flush()

  function hasAtGoogle(rowAsObj) {
    return rowAsObj["Participant Email"].indexOf("@google.com") !== -1
  }
  function isTVC(rowAsObj) {
    return hasAtGoogle(rowAsObj) ? checkTVC() : false

    function checkTVC() {
      const email = rowAsObj["Participant Email"];
      var tempPpt = email.substring(0, email.indexOf("@google.com"));
      return tvcConditional(tempPpt)

      function tvcConditional(tempPpt) {
        var { description } = getGooglerInfo(tempPpt) || { description: null }
        if (description == null) { errorString += `Googler: "${tempPpt} not Found. \n` }
        const result = (description == null || description == "Temp" || description == "Vendor") ? true : false
        return result
      }
    }
  }

  function hasValidResearcherEmail(uxrCorpEmail) {
    return (uxrCorpEmail == "" || uxrCorpEmail === undefined) ? false : (uxrCorpEmail.indexOf("[") >= 0)
  }
  function conditionalEscape(cond, message) {
    if (cond) {
      throw (message)
    }
    return false
  }

  function getHeaderStartRow(values) {

    return {
      headerRow: 4,
      // startRow: 7, // start @ row 8 
      startRow: 5,
      researcherLocation: getResearchEmail(values[0][3]),
      studyId: getStudyId(values[2][0]),
      studyName: getStudyName(values[1][0])
    }
    function getStudyName(str) {
      return getStrRemoveRegex(/Study name/gi, str)
    }
    function getStudyId(str) {
      const studyId = getStrRemoveRegex(/Study ID:/gi, str)
      Logger.log(`study id: ${studyId}`)
      const validStudyId = !["927311bb-44f4-4960-8cee-56461b7bcc17", ""].includes(studyId)
      // const validStudyId = !studyId.includes("927311bb-44f4-4960-8cee-56461b7bcc17") || studyId === "" || studyId === "Study ID:"
      // ["", "927311bb-44f4-4960-8cee-56461b7bcc17"].some(str => studyId.includes(str))
      Logger.log(`validStudyId: ${validStudyId}`)
      return validStudyId ? studyId : setStudyId(sheet.getRange(3, 1))
    }

    function getResearchEmail(str) {
      return getStrRemoveRegex(/UXR email: /gi, str)
    }
    function getStrRemoveRegex(regex, str) {
      return str.replace(regex, "")
        .replace(/\s+/gi, "")
    }
  }


  function getColumnIdxByHeader(headerIdx) {
    return function (str) {
      var headers = values[headerIdx]
      return headers.includes(str) ? headers.indexOf(str) : null
    }
  }

  function validatePartipantEmail(rowAsObj, userGroup) {
    let email = rowAsObj["Participant Email"];
    let value = rowAsObj["Perks Code Amount"]
    // let condiation = email.includes("@google.com")
    // let userGroup = email.includes("@google.com") ? "INT" : "NR";
    if (/int/gi.test(userGroup)) {
      var tempPpt = email.substring(0, email.indexOf("@google.com"));
      var isAtGoogle = email.indexOf("@google.com") !== -1
      !isAtGoogle && (errorString += "This is not a valid @google.com address. \n")
      var googlerInfo = getGooglerInfo(tempPpt)
      var description = googlerInfo !== null ? googlerInfo.description : null
      try {
        if (["Temp", "Vendor", null].some(str => description == str)) {
          errorString = errorString + "catalog type Internal, This person is a TVC. \n"
        }
      }
      catch (e) {
        let notValidEmailMessage = "This is not a valid @google.com address."
        let errorStringHasMessage = (message) => errorString.includes(message);
        !errorStringHasMessage(notValidEmailMessage) && (errorString += notValidEmailMessage + " \n")
        debugger
      }
      if (value > 65) {
        errorString = errorString + "Internal participants may not receive more than $65. \n"
      }
    }
  }

  function getSelectedSheets() {
    const perksIntId = "1EBbqAwjBtTyxOQ-BtzvyK0KCalBDlJweNm5aMEc2qLo"
    return {
      USsheetObj: getSelectedSheet("1iRNxGDH6Bj7Raf_d8mD-lp2sX6w2UMjYCgAzJN5xCiw"), // 12,28 udpate live ref
      INTsheetObj: getSelectedSheet(perksIntId),
      INToptionSheetObj: getSelectedSheet(perksIntId, "2021 Country and Currency Info"),
      INTlanguageSheetObj: getSelectedSheet(perksIntId, "Language and Currency Info"),
      massageSheetObj: getSelectedSheet("1h3un1eSB5NhIZ_MyGWsdW2hH0DClkueZkMORLUWUfi4", "Incoming - New 12/2020"),  // 12.28 update live ref
      APACSheetObj: getSelectedSheet("1pzXRDvRuY2Itp0nX_ioYOwKM9K-f6sD6MZeY1XNdqBc", "AUD/INR")
    }
    /*


    */

    function getSelectedSheet(id, sheetName) {
      const ss = SpreadsheetApp.openById(id);
      const sheet = sheetName === undefined ? getCurrentTab(ss) : ss.getSheetByName(sheetName);
      // var sheetn = sheet.getName()
      (sheet === null && (() => { throw (`sheet Name: ${sheetName} not found`) }))
      const dataRange = sheet.getDataRange();
      const values = dataRange.getValues();
      const numRows = dataRange.getNumRows();
      return {
        sheet,
        dataRange,
        values,
        numRows,
        ss
      }
    }
  }




  function removeNaN(arr) {
    for (var r = 0; r < values.length; r++) {
      for (var c = 0; c < values[r].length; c++) {
        try {
          if (isNaN(parseInt(values[r][c])) == true) {
            values[r][c] = values[r][c].trim();
          }
        }
        catch (e) {
        }
      }
    }
    return values
  }

  // function getStudyId(range) {
  function setStudyId(range) {
    let randoAlphNum = randomString(5)
    let today = new Date();
    const studyId = `UXI-IN-${today.getFullYear()}-${randoAlphNum}`;
    range.setValue(`Study ID: ${studyId}`)
    return studyId
    function randomString(length) {
      return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
    }
  }

}


function checkMergedRanges(ss) {
  ss = ss || SpreadsheetApp.getActiveSpreadsheet()
  var sheet = ss.getSheetByName("Incentives");
  var ranges = ["B10:E10", "B9:E9", "B8:E8"]
  var isCurrVersion = sheet.getRange(1, 1).getValue() == "INSTRUCTIONS"
  isCurrVersion && reMerge()

  function reMerge() {
    ranges.filter(isUnMerged)
      .map(toMerge)
  }
  function isUnMerged(a1) { return !sheet.getRange(a1).isPartOfMerge() }
  function toMerge(a1) { return sheet.getRange(a1).merge() }
}