function importIncentives(sheet){
  const url = Browser.inputBox("Please Provide Spreadsheet Url:")
  let hasHash = url.indexOf('#')
  debugger
  try{
    const ss = SpreadsheetApp.openByUrl(url)
    const importSheet = ss.getSheetByName('Incentives')
    const sheetsName = ss.getSheets().map(sheet => `"${sheet.getName()}"`).join(', ')
    !!importSheet ? findHeaderRow(sheet,url) : Browser.msgBox(`Unable to locate sheet named "Incentives" \\n found: ${sheetsName}`)
  } catch(e){
    Browser.msgBox(`Unable to acces url: ${url} error: ${e}`)
  }

function findHeaderRow(sheet, url) {
  sheet = sheet || SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
  const values = sheet.getDataRange()
    .getValues()
  const findHeaders = ["Participant Name", "Participant Email"]
  const isBlankObjRow = (props) => Object.values(props).join("") === ""
  const isHeaderRow = (arr) => findHeaders.every(header => arr.includes(header))
  const importData = getImportDataByUrl(url)


  /** modify import data to current sheet format */
  const headerIdx = values.findIndex(isHeaderRow)
  const { headerObj } = getDataBySheet(sheet, headerIdx, headerIdx + 1)
  debugger
  const anon = importData.map(transform(headerObj))
                     //    .map()
  anon.forEach( row => {
    Logger.log(row)
    Logger.log( SpreadsheetApp.getActiveSpreadsheet().getUrl())
     sheet.appendRow(row)
  })
  SpreadsheetApp.flush();
  SpreadsheetApp.getActiveSpreadsheet().toast("import complete")


  debugger
  function transform(headerObj) {
    const mapObj = getMappingObject()
    return function (props) {
      let localProp = {}
      for (let headerName in headerObj) {
        let preHeaderName = (Array.isArray(mapObj[headerName])) ? mapObj[headerName].find( str => props.hasOwnProperty(str)) : mapObj[headerName]
        // let preHeaderName = mapObj[headerName]
        let hasHeaderName =  props.hasOwnProperty(preHeaderName)
        if(hasHeaderName){
          localProp[headerName] = props[preHeaderName]
        } else{
          debugger
        }
        
      }
      return setupRowToAppend(localProp)

      function setupRowToAppend(props){
        let arr = new Array(15).fill("")
        for (let headerName in headerObj){
          let colIdx = headerObj[headerName]
          arr[colIdx] = props[headerName]
        }
       return arr
      }
    }

    function getMappingObject() {
      // {toHeaderName: fromHeaderName}

      return {
        "Participant Name": "Participant Name",
        "Participant Email": "Participant Email",
        "Office Location (Googler)*": "Office Location (if Googler)",
        "Session Type": "Session Type",
        "Incentive": "Incentive type",
        "# of massage points requested": "# of massage points requested",
        "APAC Incentive Amount": "APAC Incentive Amount",
        "Currency (APAC)": "Currency Type (apac)",
        "Perks Code Amount": "Perks Code Amount",
        "Currency": "Currency Type",
        "Participant Country Code": "Participant's Country Abbreviation",
        "Participant Preferred Language": ["Participant's Preferred Language", "Participant Preferred Language"],
        "Catalog Type": "Catalog Type",
        "Processed Status": ["Processed?", "Processed Status"],
      }
    }
  }


  function getImportDataByUrl(url) {
    let importSpreadsheet
    try{
      importSpreadsheet = SpreadsheetApp.openByUrl(url)
    } catch(e){
      Browser.msgBox(`Not a valid url ${url}`)
      throw(e)
    }


    const importUrl = url || null
    const importSheetName = "Incentives"
    const importSheet = SpreadsheetApp.openByUrl(importUrl).getSheetByName(importSheetName)
    const importValues = importSheet.getDataRange().getValues()
    const externalHeaderIdx = importValues.findIndex(isHeaderRow)
    const { body, headers } = getDataBySheet(importSheet, externalHeaderIdx, externalHeaderIdx + 1)
    const importData = body.filter(props => !isBlankObjRow(props))
    return importData
  }


  // function isHeaderRow(arr) {
  //   return findHeaders.every(header => arr.includes(header))
  // }

  /*
  getData
  
  
  function getSelectedSheets() {
    const perksIntId = "1EBbqAwjBtTyxOQ-BtzvyK0KCalBDlJweNm5aMEc2qLo"
    return {
      USsheetObj: getSelectedSheet("1iRNxGDH6Bj7Raf_d8mD-lp2sX6w2UMjYCgAzJN5xCiw"), // 12,28 udpate live ref
      INTsheetObj: getSelectedSheet(perksIntId),
      INToptionSheetObj: getSelectedSheet(perksIntId, "Country and Currency Info"),
      INTlanguageSheetObj: getSelectedSheet(perksIntId, "Language and Currency Info"),
      massageSheetObj: getSelectedSheet("1h3un1eSB5NhIZ_MyGWsdW2hH0DClkueZkMORLUWUfi4", "Incoming - New 12/2020"),  // 12.28 update live ref
      APACSheetObj: getSelectedSheet("1pzXRDvRuY2Itp0nX_ioYOwKM9K-f6sD6MZeY1XNdqBc", "AUD/INR")
    }

  */
}
}