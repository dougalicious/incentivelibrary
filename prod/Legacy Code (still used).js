// Get person info using AdminDirectory.
// All users are assumed to be registered in ldap as userName@google.com
function getGooglerInfo(userName) {
  if (userName === undefined) {
    userName='srothman';
  }

  var person = null;
  try {
    var resp = AdminDirectory.Users.get(userName + "@google.com", { 'viewType': 'domain_public' });
    if (resp != null) {
      Logger.log('AD Response Successful');
      var p = resp;
      person = {};
      // Safe mapping to bring data if it can be found under the deep nested hierarchy.
      person.id = p.id;
      person.employeeId = getExternalId(p.externalIds, "Employee ID");

      try {
        // Try to find a primary organization.  If not found, use the first one.
        var primaryOrg = getPrimaryOrganization(p.organizations);
        //person.physicalDesk = primaryOrg.location;

        person.description = primaryOrg.description;
        person.title = primaryOrg.title;

      } catch (e) { Logger.log("Organization not found for %s", userName); };

      try {
        var managerIds = p.relations
          .filter(function (r) { return r.type == "manager" })
          .map(function (r) { return getUserNameFromEmail(r.value); });
        person.managers = managerIds.join(", ");  // Comma separated list of managers
        person.manager = managerIds[0];           // First found manager
      } catch (e) { Logger.log("Admin assistants not found for %s", userName); };

    }
  }
  catch (e) {
    var exp = e;
    Logger.log(e);
    Logger.log("%s not found on` teams", userName);
  }
  return person;
}

function getExternalId(externalIds, type) {
  try {
    return externalIds.filter(function (id) { return id.customType == type; })
      .map(function (id) { return id.value; })[0];
  } catch (e) {
    Logger.log(e);
    Logger.log("External Id with type %s not found", type);
  }
}

function getUserNameFromEmail(email) {
  // Regex used to extract username from the email
  return email.match(/(("?(.*)"?)\s)?(<?((.*)@.*)>?)/i).slice(-1)[0];
}



function getPrimaryOrganization(organizations) {
  var primaryOrg = organizations.filter(function (o) { return o.primary });
  if (primaryOrg.length > 0) {
    return primaryOrg[0];
  } else {
    return organizations[0];
  }
}


function getManagerChain(ldap) {
  var managerChain = [];
  var myldap = ldap.toLowerCase();
  var found = true;

  do {
    found = false;
    try {
      myldap = getGooglerInfo(myldap).managers;
      managerChain.push(myldap);

      found = true;
      Logger.log(myldap);
    }
    catch (e) {
      found = false;
    }
  } while (found == true);
  Logger.log(managerChain);
  return managerChain;
}

function getResearcherID(researcherID, mySheet, errorString) {

  var BUSpreadsheet = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A/edit#gid=955617494");
  var BUTab = BUSpreadsheet.getSheetByName("All ldaps");
  var rows = BUTab.getDataRange();
  var values = rows.getValues();
  var found = false;

  var PATab = BUSpreadsheet.getSheetByName("PAs");
  var PArows = PATab.getDataRange();
  var PAvalues = PArows.getValues();


  if (isNaN(researcherID)) {
    researcherID = researcherID.toLowerCase();
    if (researcherID.indexOf("@google.com") >= 0) {
      var temp = researcherID;
      researcherID = temp.substring(0, temp.indexOf("@google.com"));

    }
    for (var i = 0; i < values.length; i++) {
      if (values[i][0] == researcherID) {
        return ([values[i][11], values[i][10], errorString]);
      }
    }
  }
  else {
    for (var i = 0; i < values.length; i++) {
      if (values[i][10] == researcherID) {
        return ([values[i][11], values[i][10], errorString]);
      }
    }
  }


  var managerChain = getManagerChain(researcherID);
  try {
    var job = getGooglerInfo(researcherID).title;
  }
  catch (e) {

  }

  if (!job) {
    errorString += `Cannot find ${researcherID} Researcher's ldap in Teams/Moma. \n`
    return ([, , errorString]);
  }
  var exceptionSheet = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A/edit");
  var supportedTitleTab = exceptionSheet.getSheetByName("Supported Exceptions - Titles");
  var unsupportedTitleTab = exceptionSheet.getSheetByName("Unsupported Exceptions - Titles");
  var supportedTitleArray = [];
  var unsupportedTitleArray = [];
  var supportedTitleValues = supportedTitleTab.getDataRange().getValues();
  var unsupportedTitleValues = unsupportedTitleTab.getDataRange().getValues();
  var supportedLdapsTab = exceptionSheet.getSheetByName("Supported Exceptions - LDAPs");
  var unsupportedLdapsTab = exceptionSheet.getSheetByName("Unsupported Exceptions - LDAPs");
  var supportedLdapsArray = [];
  var unsupportedLdapsArray = [];
  var supportedLdapsValues = supportedLdapsTab.getDataRange().getValues();
  var unsupportedLdapsValues = unsupportedLdapsTab.getDataRange().getValues();


  for (var s = 1; s < supportedTitleValues.length; s++) {
    supportedTitleArray.push(supportedTitleValues[s][0]);
  }

  for (var s = 1; s < unsupportedTitleValues.length; s++) {
    unsupportedTitleArray.push(unsupportedTitleValues[s][0]);
  }

  for (var s = 1; s < supportedLdapsValues.length; s++) {
    supportedLdapsArray.push(supportedLdapsValues[s][0]);
  }

  for (var s = 1; s < unsupportedLdapsValues.length; s++) {
    unsupportedLdapsArray.push(unsupportedLdapsValues[s][0]);
  }

  if (((job.toLowerCase().indexOf("research") < 0) && (supportedTitleArray.indexOf(job) < 0)) && (supportedLdapsArray.indexOf(researcherID) < 0) || ((unsupportedTitleArray.indexOf(job) >= 0) || (unsupportedLdapsArray.indexOf(researcherID) >= 0))) {
    errorString += "We do not support this researcher. \n"
    return ([, , errorString]);
  }


  for (var m = 0; m < managerChain.length; m++) {
    Logger.log(managerChain[m]);
    if (managerChain[m] != "") {
      for (var i = 1; i < PAvalues.length; i++) {
        var tempCheck = PAvalues[i][1] + ", " + PAvalues[i][10] + ",";
        if (tempCheck.indexOf(managerChain[m] + ",") >= 0) {
          Logger.log("Manager On the list! " + managerChain[m]);

          //send email to Pod Leads to let them know about the exception

          var email = "uxr-ops-pod-leads@google.com, uxi-automation@google.com";
          var subject = "Support exception (manager supported)";
          var body = "Hello Pod Leads,<br><br>" +
            "This is a notification that a support exception was made.<br>" +
            "A new incentives request has come in from <b>" + researcherID + "@</b>.<br>" +
            "This was originally flagged as not supported but since the manager, <b>" + managerChain[m] + "@</b>, is on the list, we will support.<br>" +
            "<br><br>" +
            "Thank you!<br>" +
            "Super duper allocator script Ninja <br><br>" +
            "Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>";

          //send email re-enable for 2021 launch, part of legacy code
          // SpreadsheetApp.getActiveSpreadsheet().toast("Send Email Commented out")
          try{
            Logger.log("send mail commented out")
            GmailApp.sendEmail(email, subject, body, { htmlBody: body });
          } catch(e){

          }



          return ([values[i][11], values[i][10], errorString]);
        }
      }
    }
  }



  errorString += "We do not support this researcher. \n"
  return ([, , errorString]);

};


function getCurrentTab(spreadsheet) {
  var tempSheet = spreadsheet.getSheets()[0];
  var numSheets = spreadsheet.getSheets().length;
  var today = new Date();
  var todayYear = today.getYear();
  var todayYear = Utilities.formatDate(today, "EST", "yyyy");
  var todayMonth = today.getMonth();
  for (var j = 0; j < numSheets; j++) {
    tempSheet = spreadsheet.getSheets()[j];

    if (todayMonth == 0 || todayMonth == 1 || todayMonth == 2) {
      if ((tempSheet.getName().indexOf(todayYear) >= 0) && (tempSheet.getName().indexOf("Q1") >= 0)) {
        return tempSheet;
      }

    }
    else if (todayMonth == 3 || todayMonth == 4 || todayMonth == 5) {
      if ((tempSheet.getName().indexOf(todayYear) >= 0) && (tempSheet.getName().indexOf("Q2") >= 0)) {
        return tempSheet;
      }
    }
    else if (todayMonth == 6 || todayMonth == 7 || todayMonth == 8) {
      if ((tempSheet.getName().indexOf(todayYear) >= 0) && (tempSheet.getName().indexOf("Q3") >= 0)) {
        return tempSheet;
      }
    }
    else if (todayMonth == 9 || todayMonth == 10 || todayMonth == 11) {
      if ((tempSheet.getName().indexOf(todayYear) >= 0) && (tempSheet.getName().indexOf("Q4") >= 0)) {
        return tempSheet;
      }
    }
  }
}