function getIncentiveVersion(){
  let versionNum = 20
  return versionNum
   
}

  /* returns properties of sheet
   * @param Object({Sheet})
   * @return { dataRange, body, headerObj, header, getValueByHeader }
   */
  function getDataBySheet(sheet, headerIdx = 0, bodyRowStart = 1) {
    var dataRange = sheet.getDataRange();
    var values = dataRange.getValues()
    var headers = values.slice(headerIdx, headerIdx + 1)[0]
    var body = values.slice(bodyRowStart)
    var headerObj = headers.reduce(makeHeaderObj, {})
    var a = body.map(getValueByHeader)
    return {
      dataRange,
      body: body.map(getValueByHeader),
      headerObj,
      headers,
      getValueByHeader,
    }
    function makeHeaderObj(acc, next, idx) {
      if (next === "" || next === "-") {
        return acc;
      }
      if (acc.hasOwnProperty(next)) {
        throw ("Duplicate headers found")
      }
      acc[next] = idx
      return acc;
    }
    // transform Array(row) => Object()
    function getValueByHeader(row, rowIdx) {
      var rowAsObject = Object.create(headerObj)
      for (var header in headerObj) {
        var idx = headerObj[header]
        rowAsObject[header] = row[idx]
      }
      Object.defineProperties(rowAsObject, {
        getRangeByColHeader: {
          value: ((header) => headers.includes(header) ? sheet.getRange(rowIdx + 1 + bodyRowStart, headerObj[header] + 1) : (()=> {throw (`header "${header}" not found `)})() ),
          writable: false,
          enumerable: false,
        },
      })
      Object.defineProperties(rowAsObject, {
        getRowIdx: {
          value: (sheet.getRange(rowIdx + 1 + bodyRowStart, headerObj[header] + 1).getRowIndex()), // return range of column 4
          writable: false,
          enumerable: false,
        },
      })

      return rowAsObject
    }
  }